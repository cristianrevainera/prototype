package com.example.gof.prototype;

public class NotebookComputer extends Computer {
        private String touchPad;

        public NotebookComputer(String cpu, String ram, String harddisk, String screen, String keyboard, String touchPad) {
                super(cpu, ram, harddisk, screen, keyboard);
                this.touchPad = touchPad;
        }

        public String getTouchPad() {
            return this.touchPad;
        }

        public String getTouchPad(String touchPad) {
            return this.touchPad = touchPad;
        }
}