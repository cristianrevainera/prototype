package com.example.gof.prototype;

public class Test {

    public static void main(String args[]) throws CloneNotSupportedException {
        ComputerPrototype computerPrototype = new ComputerPrototype();

        DesktopComputer desktop1 = (DesktopComputer) computerPrototype.prototype("desktop");
        System.out.println(desktop1); // com.example.gof.prototype.DesktopComputer@4554617c
        System.out.println("Procesor: "+ desktop1.getCpu() + " Mouse: " + desktop1.getMouse());
        // Procesor: I3 Mouse: Logtec

        DesktopComputer desktop2 = (DesktopComputer) computerPrototype.prototype("desktop");
        System.out.println(desktop2);  // com.example.gof.prototype.DesktopComputer@74a14482
        System.out.println("Procesor: "+ desktop2.getCpu() + " Mouse: " + desktop2.getMouse());
        // Procesor: I3 Mouse: Logtec

        NotebookComputer notebookComputer1 = (NotebookComputer) computerPrototype.prototype("notebook");
        System.out.println(notebookComputer1); // com.example.gof.prototype.NotebookComputer@1540e19d
        System.out.println("Procesor: "+ notebookComputer1.getCpu() + " TouchPad: " + notebookComputer1.getTouchPad());
        // Procesor: I3 TouchPad: Resisteve touchpad

        NotebookComputer notebookComputer2 = (NotebookComputer) computerPrototype.prototype("notebook");
        System.out.println(notebookComputer2); // com.example.gof.prototype.NotebookComputer@677327b6
        System.out.println("Procesor: "+ notebookComputer2.getCpu() + " TouchPad: " + notebookComputer2.getTouchPad());
        // Procesor: I3 TouchPad: Resisteve touchpad
    }
}
