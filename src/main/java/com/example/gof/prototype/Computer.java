package com.example.gof.prototype;


public abstract class Computer implements Cloneable {
    private String cpu;
    private String ram;
    private String harddisk;
    private String screen;
    private String keyboard;

    public Computer(String cpu, String ram, String harddisk, String screen, String keyboard) {
        this.cpu = cpu;
        this.ram = ram;
        this.harddisk = harddisk;
        this.screen = screen;
        this.keyboard = keyboard;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getHarddisk() {
        return harddisk;
    }

    public void setHarddisk(String harddisk) {
        this.harddisk = harddisk;
    }

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

}