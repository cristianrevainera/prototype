package com.example.gof.prototype;

import java.util.HashMap;

public class ComputerPrototype {
    private HashMap<String, Computer> prototypes = new HashMap<String, Computer>();

    public ComputerPrototype() {
        DesktopComputer  desktop = new DesktopComputer("I3", "4gb", "1TB", "17 Pulgadas",
                "101 teclas español", "Logtec");

        NotebookComputer notebook = new NotebookComputer("I3", "4GB", "500MB", "14 pulgadas",
                "101 teclas español", "Resisteve touchpad");

        prototypes.put("desktop", desktop);
        prototypes.put("notebook", notebook);
    }

    public Computer prototype(String type) throws CloneNotSupportedException {
        return (Computer) prototypes.get(type).clone();
    }
}
