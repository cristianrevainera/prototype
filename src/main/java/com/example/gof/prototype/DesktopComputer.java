package com.example.gof.prototype;

public class DesktopComputer extends Computer {
    private String mouse;

    public DesktopComputer(String cpu, String ram, String harddisk, String screen, String keyboard, String mouse) {
        super(cpu, ram, harddisk, screen, keyboard);
        this.mouse = mouse;
    }

    public String getMouse() {
        return mouse;
    }

    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

}